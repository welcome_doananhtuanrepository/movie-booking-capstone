import { useEffect, useState } from "react";
import Swal from "sweetalert2";

const handleTimeChange=(t)=>{
    if(!t) return 0;
    const left=t-new Date().getTime()
    if(left<0){
        Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: 'Đã hết thời gian chờ đặt vé !',
            footer: '<a href="">Đặt lại</a>',
            showConfirmButton:false
          })
        return 0
    }
    return left
}

export default function useCountDown(){
    
    const [endTime, setEndTime]=useState(0)
    const [timeChange,setTimeChange]=useState(()=>handleTimeChange(endTime))
    useEffect(()=>{
        setTimeChange(handleTimeChange(endTime))
        const idTimer=setInterval(()=>{
            setTimeChange(handleTimeChange(endTime))
            if(handleTimeChange(endTime)===0){
                clearInterval(idTimer)
            }
        })
    return ()=>clearInterval(idTimer)
    },[endTime])
    return [timeChange, setEndTime]
}