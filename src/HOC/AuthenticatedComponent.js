import React from 'react'
import { useNavigate, Navigate } from 'react-router-dom'
import { userInforLocal } from '../service/local.sevice'

export default function AuthenticatedComponent({children}) {
    const navigate = useNavigate()
    // console.log('user', userInforLocal.get());
    // console.log('navigate', navigate)

    let user = userInforLocal.get()
    console.log(user)
    if (user === null || user.maLoaiNguoiDung !== "QuanTri") {
          console.log('effect')
            navigate("/");
            // window.location.href = "/";
            // userInforLocal.remove()
            return <Navigate to="/"  />
        }

  return (
    children
  )
}
