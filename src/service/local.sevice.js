
let UserInfor="UserInfor"
export let userInforLocal={
    set:(userData)=>{
        let json=JSON.stringify(userData)
        localStorage.setItem(UserInfor,json)
    },
    get:()=>{
        const json=localStorage.getItem(UserInfor)
        if(json){
            return JSON.parse(json)
        }else{
            return
        }
    },
    remove:()=>{
        localStorage.removeItem(UserInfor)
    }
}