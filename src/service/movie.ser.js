
import { https } from "./axiosConfig"

export const movieServ={
    getBanner: ()=>{
        return https.get ("/api/QuanLyPhim/LayDanhSachBanner")
    },

    getListMovies: ()=>{
        return https.get ("/api/QuanLyPhim/LayDanhSachPhim?maNhom=GP05")
    },

    getMoviesbyTheater:()=>{
        return https.get ("/api/QuanLyRap/LayThongTinLichChieuHeThongRap?maNhom=GP05")
    },

    getMoviesDetail:(id)=>{
        return https.get (`api/QuanLyRap/LayThongTinLichChieuPhim?MaPhim=${id}`)
    },
    getSeatDetail:(id)=>{
        return https.get (`api/QuanLyDatVe/LayDanhSachPhongVe?MaLichChieu=${id}`)
    },
    removeMovie: (idMovie) => {
        let uri = `/api/QuanLyPhim/XoaPhim/${idMovie}`;
        return https.delete(uri);
      },
      postMovie: (dataForm) => {
        let uri = "/api/QuanLyPhim/ThemPhimUploadHinh";
        return https.post(uri, dataForm);
      },
}