import axios from "axios"
import { BASE_URL, configHeaders } from "../service/axiosConfig"


export const userServ={
    postLogin: (dataLogin)=>{
        return axios({
            url:`${BASE_URL}/api/QuanLyNguoiDung/DangNhap`,
            method:"POST",
            data:dataLogin,
            headers: configHeaders()
        })
    },
    postSign: (dataSign)=>{
        return axios({
            url:`${BASE_URL}/api/QuanLyNguoiDung/DangKy`,
            method:"POST",
            data:dataSign,
            headers: configHeaders()
        })
    }
}