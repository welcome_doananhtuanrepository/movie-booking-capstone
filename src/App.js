import { Suspense, lazy } from "react";

import {BrowserRouter,Routes,Route} from "react-router-dom";

import Loading from "./pages/loading/Loading";
// import TicketRoom from "./pages/ticketroom/TicketRoom";
import AuthenticatedComponent from "./HOC/AuthenticatedComponent";
import MainPage from "./pages/Mainpage/Mainpage";
const Home = lazy(() => import("./components/Home/Home"));
const SignIn = lazy(() => import("./pages/dangky/Signin"));
const Login = lazy(() => import("./pages/dangnhap/Login"));
const Phim = lazy(() => import("./pages/phim/Phim"));
const Booking= lazy(() => import("./pages/detail/booking"));

function App() {
  return (
    <div className="font-main overflow-hidden">
      <BrowserRouter>
          <Routes>
              <Route path="/" element={
                <Suspense fallback={<Loading/>}>
                    <Home/>
                </Suspense>
              }/>
              <Route path="/dangky" element={
                <Suspense fallback={<Loading/>}>
                    <SignIn/>
                </Suspense>
              }></Route>

              <Route path="/dangnhap" element={
                <Suspense fallback={<Loading/>}>
                    <Login/>
                </Suspense>
              }></Route>

              <Route path="/phim/:maPhim" element={
                <Suspense fallback={<Loading/>}>
                    <Phim/>
                </Suspense>
              }></Route>
            
              <Route path="/datve/:maLichChieu" element={
                <Suspense fallback={<Loading/>}>
                    <Booking/>
                </Suspense>
              }></Route>

              <Route path="/main-page" element={
                // <Suspense fallback={<Loading/>}>
                    (<AuthenticatedComponent>
                        <MainPage />
                    </AuthenticatedComponent>)
                // </Suspense>
              }></Route>
             
          </Routes>
        
      </BrowserRouter>
      
    </div>
  );
}

export default App;
