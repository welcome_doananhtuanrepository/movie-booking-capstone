import { message } from "antd";
import {  useNavigate } from "react-router-dom";
import { userInforLocal } from "../../services/local.sevice";
import { userServ } from "../../services/userServ";
import { SET_LOGIN } from "../constant/userConstant";

export const setLoginActionSer=(values,onSuccess)=>{
    return (dispatch)=>{
        userServ
        .postLogin(values)
        .then((res) => {
          console.log(res);
          // lưu thông tin đăng nhập xuống localStorage
          userInforLocal.set(res.data.content);
          dispatch({
            type: SET_LOGIN,
            payload: res.data.content,
          });
          onSuccess()
        })
        .catch((err) => {
          message.error(err.response.data.content);
          console.log(err);
        });
    }
    
}