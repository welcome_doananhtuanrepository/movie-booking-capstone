import { combineReducers } from "redux";
import { userReducer } from "./userReducer";
import {moviesReducer} from "./movieReducer";
import { bookingReducer } from "./BookingReducer";
export const rootReducer=combineReducers({
    userReducer,
    moviesReducer,
    bookingReducer
})