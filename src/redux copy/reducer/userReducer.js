import { userInforLocal } from "../../service/local.sevice";
import {SET_LOGIN} from "../constant/userConstant"  

let initialState={
    userInfor:userInforLocal.get()
    // userInfor:null
}

export let userReducer = (state = initialState, { type, payload }) => {
    switch (type) {
      case SET_LOGIN: {
        return { ...state, userInfor:payload};
      }
      default:
        return state;
    }
  };
  