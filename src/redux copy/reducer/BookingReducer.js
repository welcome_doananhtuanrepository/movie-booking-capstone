import { SELECT_PAYMENT_METHOD, SELECT_SEAT } from "../constant/BookingMovie";

const initialState = {
  selectedSeats: [],
  totalAmount: 0,
  paymentMethod: "",
};

export const bookingReducer = (state = initialState, { type, payload }) => {
  console.log("bookingReducer", type, payload);
  switch (type) {
    case SELECT_SEAT: {
      // find index seat id
      if(payload===null){
        return {
          ...state,
          selectedSeats:[]
        }
      }
      let index = state.selectedSeats.findIndex((item) => {
        return item.maGhe === payload.maGhe;
      });
      let newSelectedSeats = [...state.selectedSeats];
      let newTotalAmount = 0;
      if (index == -1) {
        let newSeat = { ...payload, loaiGhe: "DangChon" };
        newSelectedSeats.push(newSeat);
        console.log(newSelectedSeats);
        newSelectedSeats.map((item) => {
          newTotalAmount += item.giaVe;
        });
        console.log(newTotalAmount);
      } else {
        return { ...state };
      }
      return {
        ...state,
        selectedSeats: newSelectedSeats,
        totalAmount: newTotalAmount,
      };
    }
    case SELECT_PAYMENT_METHOD: {
      return {
        ...state,
        paymentMethod: payload,
      };
    }
    default:
      return state;
  }
};
