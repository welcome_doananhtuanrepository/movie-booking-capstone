import React from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import { SELECT_SEAT } from '../../redux copy/constant/BookingMovie'

export default function Checkout({setCurrent,inforPhim}) {
    const dispatch=useDispatch()
    const selectedSeats=useSelector(state=>state.bookingReducer.selectedSeats)
    const totalAmount=useSelector(state=>state.bookingReducer.totalAmount)
  const navigate=useNavigate();
    const handleConfirm=()=>{
        setCurrent(0)
        window.location.reload()
      }
    const handleBack=()=>{
        setCurrent(0)
        dispatch({type:SELECT_SEAT, payload:null})
        navigate("/")
    }
  return (
    <div className="w-full h-screen fixed top-0 left-0 z-10 bg-[rgba(228,220,220,0.4)]">
          <div className="bg-[#fefefe] mx-auto flex flex-col my-32 p-[20px] border lg:w-[750px] md:w-[650px] sm:w-[550px] w-[350px]">
            <div className="flex flex-col gap-2">
              <div>
                <span className="text-[16px] font-semibold">Tên phim: </span><span className="text-[16px]">{inforPhim.tenPhim}</span>
              </div>
              <div>
                <span className="text-[16px] font-semibold">Địa chỉ: </span><span className="text-[16px]">{inforPhim.diaChi}</span>
              </div>
              <div>
                <span className="text-[16px] font-semibold">Thời gian: </span><span className="text-[16px]">{inforPhim.gioChieu} - {inforPhim.ngayChieu}</span>
              </div>
              <div>
                <span className="text-[16px] font-semibold">Số ghế: </span>
                <span className="font-medium text-lg text-[16px]">
                {selectedSeats.map((item) => {
                  let content = "";
                  let contentHTML = "";
                  content = `Ghế ${item.tenGhe}, `;
                  return (contentHTML += content);
              })}
                </span>
              </div>
              <div>
                <span className="text-[16px] font-semibold">Tổng tiền: </span><span className="text-[16px]">{totalAmount}đ</span>
              </div>
              <div className='flex  justify-center gap-4'>
              <button onClick={handleConfirm} className="mt-2 md:text-[16px]  text-white bg-gradient-to-r from-[#fbbd61] to-[#ec7532] cursor-pointer p-2 rounded-full">
                Tiếp tục đặt vé
              </button>
              <button onClick={handleBack} className="mt-2 md:text-[16px] text-white bg-gradient-to-r from-[#fbbd61] to-[#ec7532] cursor-pointer p-2 rounded-full">
                Quay lại trang chủ
              </button>
              </div>
            </div>
          </div>
      </div>
  )
}
