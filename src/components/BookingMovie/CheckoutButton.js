import React from "react";

export default function CheckoutButton({ next }) {
  // console.log(next);
  return (
      <button
        onClick={() => {
          next();
        }}
        className="mt-2 text-[20px] text-white w-full bg-gradient-to-r from-[#fbbd61] to-[#ec7532] cursor-pointer p-3 rounded-full"
      >
        Kiểm tra đặt vé
      </button>
  );
}
