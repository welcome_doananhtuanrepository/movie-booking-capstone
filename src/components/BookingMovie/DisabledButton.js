import React from "react";
import { Button } from "antd";

export default function DisabledButton() {
  return (
      <button
        type="button"
        disabled
        className="mt-2 text-[20px] text-white w-full bg-slate-300 cursor-pointer p-3 rounded-full"
      >
        ĐẶT VÉ
      </button>
  );
}
