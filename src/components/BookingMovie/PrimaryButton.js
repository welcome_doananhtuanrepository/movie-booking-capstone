import { message } from "antd";
import React from "react";

export default function PrimaryButton({ next }) {
  // console.log(next);
  return (
      <button
        onClick={() => {
          message.success("Đặt vé thành công!")
          next();
        }}
        className="mt-2 text-[20px] text-white w-full bg-gradient-to-r from-[#fbbd61] to-[#ec7532] cursor-pointer p-3 rounded-full"
      >
        ĐẶT VÉ
      </button>
  );
}
