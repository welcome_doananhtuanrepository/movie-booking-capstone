import React from "react";

export default function UnavaiSeat() {
  return (
    <div>
      <div className="flex flex-col items-center relative">
        <div className="sm:w-7 sm:h-5 w-5 h-4 bg-blue-300 rounded-sm"></div>
        <div className="absolute bottom-1 sm:w-5 sm:h-1 w-3 h-1 bg-slate-100 rounded-sm"></div>
      </div>
    </div>
  );
}
