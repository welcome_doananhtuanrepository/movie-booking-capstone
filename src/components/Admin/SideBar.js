import { UserOutlined, VideoCameraAddOutlined } from '@ant-design/icons';
import { Menu } from 'antd';
import Sider from 'antd/lib/layout/Sider';
import React, { useState } from 'react'
import "../../pages/UserManagement/UserManagement.css"

export default function SideBar({getCurrentKey}) {
  const [key, setKey] = useState('1')
  const onClick = (e) => {
    setKey(e.key)
    getCurrentKey(e.key)
  }
  
  const items = [
    {
      key: '1',
      icon: <UserOutlined />,
      label: 'Quản Lý Người Dùng',
    },
    {
      key: '2',
      icon: <VideoCameraAddOutlined />,
      label: 'Quản Lý Người Phim',
    }
  ];
  return (
    <Sider
      breakpoint="lg"
      collapsedWidth="0"
      onBreakpoint={(broken) => {
        console.log(broken);
      }}
      onCollapse={(collapsed, type) => {
        console.log(collapsed, type);
      }}
    >
      <div className="logo" />
      <Menu
        onClick={onClick}
        theme="dark"
        mode="inline"
        defaultSelectedKeys={['1']}
        selectedKeys={[key]}
        items={items}
      />
    </Sider>
  )
}
