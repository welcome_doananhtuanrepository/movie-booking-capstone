import React from "react";
import Layout, { Content, Header } from "antd/lib/layout/layout";
import "../../pages/UserManagement/UserManagement.css";
import { Button } from "antd";
import { LogoutOutlined } from "@ant-design/icons";
import { userInforLocal } from "../../service/local.sevice";

export default function HeaderApp() {
  const userInfor = userInforLocal.get();
  // console.log(userInfor);
  return (
      <Header
        className="site-layout-sub-header-background"
        style={{
          padding: 0,
        }}
      >
        <div className="flex flex-row justify-end items-center">
          <p className="text-black mr-5"> User:{userInfor.content.hoTen} </p>
          <Button icon={<LogoutOutlined />}>Đăng xuất</Button>
        </div>
      </Header>
  );
}
