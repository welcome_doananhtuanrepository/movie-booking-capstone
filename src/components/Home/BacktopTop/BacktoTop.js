import React, { useEffect, useState } from 'react'
import { AiOutlineArrowUp } from 'react-icons/ai'

export default function BacktoTop() {
    const [show, setShow]=useState(false)
    useEffect(()=>{
        const handleScroll=()=>{
            if(window.scrollY>=200){
                setShow(true)
            }else{
                setShow(false)
            }
        }
        window.addEventListener("scroll",handleScroll)
        return ()=>{
            window.removeEventListener("scroll",handleScroll)
        }
    },[])
  return (
    <a href='#Trang chủ' className={`fixed ${show?"":"hidden"} rounded cursor-pointer bg-gradient-to-r from-orange-400 to-orange-700 w-12 h-12 bottom-6 right-6 flex items-center justify-center`}>
        <AiOutlineArrowUp size={"30px"} color="white"/>
    </a>
  )
}
