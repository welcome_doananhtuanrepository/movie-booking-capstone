import React, { useEffect, useState } from "react";
import { BsFillPlayFill } from "react-icons/bs";

function Header() {
  const [isRadio, setIsRadio] = useState(1);
  const [isAnimation, setIsAnimation] = useState(true);
  const handlerSelectRadio = (e) => {
    setIsRadio(+e.currentTarget.value);
  };
  const [isShowVideo, setIsShowVideo] = useState(false);
  // 
  return (
    <div
      className={
        isRadio === 1 ? `w-full h-[660px] bg-cover bg-[url("https://movienew.cybersoft.edu.vn/hinhanh/ban-tay-diet-quy.png")] relative bg-center`
        : isRadio === 2 ? `w-full h-[660px] bg-cover bg-[url("https://movienew.cybersoft.edu.vn/hinhanh/lat-mat-48h.png")] relative bg-center`
        : `w-full h-[660px] bg-cover bg-[url("https://movienew.cybersoft.edu.vn/hinhanh/cuoc-chien-sinh-tu.png")] relative bg-center`
      }>
        {isShowVideo && (
        <div
          onClick={() => setIsShowVideo(false)}
          className="fixed inset-0 bg-black/80 z-30 flex justify-center items-center"
        >
          <div className="relative lg:w-[960px] lg:h-[540px] md:w-[640px] md:h-[360px] h-[260px] w-full p-8">
            <iframe
              className="absolute inset-0 w-[calc(100%-2rem)] h-full left-1/2 transform -translate-x-1/2"
              src={isRadio===1&&isShowVideo?`https://www.youtube.com/embed/uqJ9u7GSaYM`:
              isRadio===2&&isShowVideo?`https://www.youtube.com/embed/kBY2k3G6LsM`:
              `https://www.youtube.com/embed/_rUC3-pNLyc`}
              title="YouTube video player"
              frameBorder="0"
              allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
              allowfullscreen
            ></iframe>
          </div>
        </div>
      )}
      <div className="inset-0 bg-gradient-to-b from-transparent to-black absolute"></div>
      <div className="max-w-[90%] my-0 mx-auto mt-4 p-2 md:p-0">
        <div className="absolute top-[35%]">
          <div
            onAnimationEnd={() => setIsAnimation(false)}
            className={`${isAnimation ? "animate-fade-in-down-delay" : ""} mb-4`}
          >
            <span className="text-[#fbbd61] font-small ">ACTION, ADVENTURE, FANTASY</span>
          </div>
          <div
            className={`text-4xl md:text-5xl lg:text-6xl text-[#fff] font-[500] ${
              isAnimation ? "animate-fade-in-down" : ""
            }`}
          >
            {isRadio === 1
              ? "Bàn tay diệt quỷ"
              : isRadio === 2
              ? "Lật mặt"
              : isRadio === 3
              ? "Cuộc chiến sinh tử"
              : null}
          </div>
          <div className={`text-[#fff] font-medium mt-4 ${isAnimation ? "animate-fade-in-up" : ""}`}>
            Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium lectorum. Mirum est notare quam{" "}
            <br /> littera gothica, quam nunc putamu.
          </div>
          <div className={`flex items-center mt-8 ${isAnimation ? "animate-fade-in-up-delay" : ""}`}>
            <div className="w-[50px] h-[50px] rounded-[50%] border-[2px] border-white relative mr-[10px]">
              <span className="absolute top-[50%] left-[50%] translate-x-[-50%] translate-y-[-50%] text-[#fff]">
                PG
              </span>
            </div>
            <div className="button bg-gradient-to-r from-[#fbbd61] to-[#ec7532] cursor-pointer p-3 rounded-full">
              <div className="text-white flex items-center min-w-[192px] justify-center">
                <span className="flex items-center text-xl mr-2 z-10">
                  <BsFillPlayFill />
                </span>
                <span className="text-sm tracking-[0.3rem] z-10" onClick={()=>setIsShowVideo(true)}>PLAY TRAILER</span>
              </div>
            </div>
          </div>
        </div>
        <div className="flex items-center absolute bottom-[110px]">
          <input
            type="radio"
            className="w-4 h-4 mx-[5px]"
            name="image1"
            onChange={handlerSelectRadio}
            value="1"
            onClick={() => setIsAnimation(true)}
            checked={isRadio === 1}
          />
          <input
            type="radio"
            className="w-4 h-4 mx-[5px]"
            name="image1"
            onChange={handlerSelectRadio}
            value="2"
            onClick={() => setIsAnimation(true)}
            checked={isRadio === 2}
          />
          <input
            type="radio"
            className="w-4 h-4 mx-[5px]"
            name="image1"
            onChange={handlerSelectRadio}
            value="3"
            onClick={() => setIsAnimation(true)}
            checked={isRadio === 3}
          />
        </div>
      </div>
    </div>
  );
}

export default Header;
