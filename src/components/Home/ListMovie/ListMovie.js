import { message } from "antd";
import React, { useEffect, useState } from 'react'
import { AiFillStar } from 'react-icons/ai';
import { useDispatch, useSelector } from "react-redux";
import { Link } from 'react-router-dom';
import { GET_MOVIES } from "../../../redux copy/constant/userConstant";
import { movieServ } from "../../../service/movie.ser";
import "./style.css"
export default function ListMovie() {

    const [activeTabBtn,setActiveTabBtn]=useState(true)
    const [currentMovies,setCurrentMovies]=useState([])
    const [comingMovies,setComingMovies]=useState([])
   
    const movies=useSelector(state=>state.moviesReducer)
    
    const dispatch=useDispatch()

    const fetchBanner = () => {
      movieServ
        .getListMovies()
        .then((res) => {
         
          dispatch({
            type:GET_MOVIES,
            payload:res.data.content
          })
        })
        .catch((err) => {
          message.error(err.response.data.content);
        });
    };
    useEffect(()=>{
      fetchBanner()
    },[])
    
    
    const [isShowVideo, setIsShowVideo] = useState(false);
  
    const [link,setLink]=useState("krgcyk2rjFc")

    useEffect(() => {
      const willStart=movies.filter(phim=>phim.sapChieu===true)
      setComingMovies(willStart)

      const Start=movies.filter(phim=>phim.sapChieu!==true)
      setCurrentMovies(Start)

    },[movies]);
    
    const getVideoId = (urlYoutube) => {
      let videoId
      const indexLastSlash = urlYoutube.lastIndexOf("/")
      
      const resultSliceFromSlash = urlYoutube?.slice(indexLastSlash + 1)
      console.log(resultSliceFromSlash)
      videoId = resultSliceFromSlash
      const findWatch = resultSliceFromSlash?.indexOf("watch");
      if (findWatch !== -1) {
        const indexLastEqual = resultSliceFromSlash?.lastIndexOf("=")
        videoId = resultSliceFromSlash?.slice(indexLastEqual + 1);
      }
      return videoId
    } 
    const handleShowVideo=(movie)=>{
      setIsShowVideo(true)
      const link=getVideoId(movie.trailer)
      setLink(link)
    }
  return (
    <div className="container mx-auto" id="Lịch chiếu">
      
       {isShowVideo && (
        <div
          onClick={() => setIsShowVideo(false)}
          className="fixed inset-0 bg-black/80 z-30 flex justify-center items-center"
        >
          <div className="relative lg:w-[960px] lg:h-[540px] md:w-[640px] md:h-[360px] h-[260px] w-full p-8">
            <iframe
              className="absolute inset-0 w-[calc(100%-2rem)] h-full left-1/2 transform -translate-x-1/2"
              src={isShowVideo&&`https://www.youtube.com/embed/${link}`}
              title="YouTube video player"
              frameBorder="0"
              allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
              allowFullScreen
            ></iframe>
          </div>
        </div>
      )}
        <div className='flex items-center justify-center flex-col mt-6'>
            <div className='flex text-center mt-6 space-x-8 text-[24px]'>
                <h2 onClick={()=>setActiveTabBtn(true)} 
                className={`tabButton hover:text-[30px] ${activeTabBtn?"text-orange-600":""}`}>
                    Đang chiếu
                </h2>
                <h2 onClick={()=>setActiveTabBtn(false)}
                className={`tabButton hover:text-[30px] ${!activeTabBtn?"text-orange-600":""}`}>
                    Sắp chiếu
                </h2>
            </div>
        </div>
        
        <div className='mt-6 grid md:grid-cols-4 sm:grid-cols-3 grid-cols-2 gap-1'>
          
            {activeTabBtn&&currentMovies.map(movie=>(
              <div key={movie.maPhim} className='relative group p-4' >
                <div className='relative'>
                  <img className='w-full object-cover h-[250px]' src={movie.hinhAnh} alt=''></img>
                  <div className='scale-75 md:scale-100 absolute top-[12px] right-[12px] w-[54px] border border-[#1f2e46] p-1 text-center rounded-md bg-[#0c1b36cc]'>
                    <h3 className='text-white text-[18px] font-semibold'>10</h3>
                    <div className='flex text-red-500'>
                      <AiFillStar/>
                      <AiFillStar/>
                      <AiFillStar/>
                      <AiFillStar/>
                      <AiFillStar/>
                    </div>
                  </div>
                </div> 
                <div className='text-center'>
                  <h3 className='text-[16px] md:text-[23px] md:mt-2 mt-0'>{movie.tenPhim}</h3>
                  <p className='text-[13px] md:text-[16px]'>120 phút</p>
                </div>
                <div className='absolute top-0 left-0 w-[100%] h-[100%] cursor-pointer overlay'>
                  <div className='w-[100%] h-[78%] flex items-center justify-center' onClick={()=>handleShowVideo(movie)}>
                    <img src="/img/carousel/play-video.png" className='w-[50px]' alt=''/>
                  </div>
                  <Link to={`/phim/${movie.maPhim}`}><div className='flex items-center justify-center h-[19%] bg-[#fb4226] mx-4 mb-3'><h1 className='text-white text-[24px] md:text-[34px] mb-0'>Mua vé</h1></div></Link>
                </div>
            </div>
            ))}
            {!activeTabBtn&&comingMovies.map(movie=>(
              <div key={movie.maPhim} className='relative group p-4' >
                <div className='relative'>
                  <img className='w-full object-cover h-[300px]' src={movie.hinhAnh} alt=''></img>
                  <div className='scale-75 md:scale-100 absolute top-[12px] right-[12px] w-[54px] border border-[#1f2e46] p-1 text-center rounded-md bg-[#0c1b36cc]'>
                    <h3 className='text-white text-[18px] font-semibold'>10</h3>
                    <div className='flex text-red-500'>
                      <AiFillStar/>
                      <AiFillStar/>
                      <AiFillStar/>
                      <AiFillStar/>
                      <AiFillStar/>
                    </div>
                  </div>
                </div> 
                <div className='text-center'>
                  <h3 className='text-[16px] md:text-[23px] md:mt-2 mt-0'>{movie.tenPhim}</h3>
                  <p className='text-[13px] md:text-[16px]'>120 phút</p>
                </div>
                <div className='absolute top-0 left-0 w-[100%] h-[100%] cursor-pointer overlay'>
                  <div className='w-[100%] h-[78%] flex items-center justify-center' onClick={()=>handleShowVideo(movie)}>
                    <img src="/img/carousel/play-video.png" className='w-[50px]' alt=''/>
                  </div>
                  <Link to={`/phim/${movie.maPhim}`}><div className='flex items-center justify-center h-[19%] bg-[#1ecae9] mx-4 mb-3'><h1 className='text-white text-[24px] md:text-[34px] mb-0'>Thông tin phim</h1></div></Link>
                </div>
            </div>
            ))}
        </div>      
    </div>
  )
}
