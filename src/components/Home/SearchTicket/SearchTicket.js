import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import moment from "moment"
import { movieServ } from "../../../service/movie.ser";
import dateTime from "./dateJs";
import { useNavigate } from "react-router-dom";
import lodashIsEmpty from "lodash.isempty";
import { message } from "antd";
const SearchTicket = () => {
  const navigate = useNavigate();
  const listMovies = useSelector((state) => state.moviesReducer);
  const userInfo=useSelector(state=>state.userReducer.userInfor)
  // time select
  const [showTimes, setShowTimes] = useState("");

  // theater Select
  const [theater, setTheater] = useState("");
  const [theatersData, setTheatersData] = useState([]);

  // day Select
  const [showDays, setShowDays] = useState("");
  const [showDaysData, setShowDaysData] = useState([]);
  const [dateList, setDateList] = useState([]);

  // Input
  const [movieSchedule, setMovieSchedule] = useState({});
  const [maPhim,setMaphim]=useState(0)

  const handleChange = (e) => {
    setMaphim(e.target.value);
  };

  const fetchMoviesTheaters = () => {
    movieServ
      .getMoviesDetail(maPhim)
      .then((res) => {
        console.log(res.data.content)
        setMovieSchedule(res.data.content)
      })
      .catch((err) => {
        console.log(err.response.data.content);
      });
  };

  useEffect(()=>{
    fetchMoviesTheaters()
  },[maPhim])

  useEffect(() => {
    if (!movieSchedule) return;
    const res = movieSchedule.heThongRapChieu?.reduce((currVal, nextVal) => {
      const result = nextVal.cumRapChieu?.map((item) => ({
        ...item,
      }));
      return [...currVal, ...result];
    }, []);

    setTheatersData(res);
    setTheater("");
  }, [movieSchedule]);

  useEffect(() => {
    setShowDays("");
    if (lodashIsEmpty(theater)) return;
    const res = theatersData.find((item) => item.maCumRap === theater);
  
    const cloneRes = [...res.lichChieuPhim];
    
    const reDate = cloneRes.map((item) => item.ngayChieuGioChieu.slice(0, 10));
    console.log(new Set([...reDate]))
    setDateList(new Set([...reDate]));

    setShowDaysData(res.lichChieuPhim);
  }, [theater]);

  const handleChangeTheater = (event) => {
    setTheater(event.target.value);
  };

  const handleOnChangeDays = (event) => {
    setShowDays(event.target.value);
    
  };

  const handleChangeShowTime = (event) => {
    console.log(event.target.value)
    setShowTimes(event.target.value);
  };

  const handleBooking=()=>{
    if(userInfo){
      navigate(`/datve/${showTimes}`)
    }else{
      message.info("Bạn cần đăng nhập để đặt vé")
      navigate("/") 
    }
    
  }

  const renderTimes = () => {
    const timesFilter = showDaysData.filter((time) =>
      time.ngayChieuGioChieu.match(showDays)
    );
 
    return timesFilter.map((time) => (
      <option key={time.maLichChieu} value={time.maLichChieu}>
          {dateTime(time.ngayChieuGioChieu).getDateTime()}
      </option>
    ));
  };

  return (
    <div className="container lg:block hidden">
        <div className="makeStyles-search-57 mx-0 mt-[30px] w-full flex items-center justify-evenly bg-slate-100">
            <select onChange={handleChange} name="cars" className="w-[250px] text-[gray] h-10 border rounded text-[16px] bg-slate-200">
                <option className="p-5" value="0">Chọn phim</option>
                {listMovies.map(phim=>(
                    <option key={phim.maPhim} value={`${phim.maPhim}`}>{phim.tenPhim}</option>
                ))}
            </select>

            <select onChange={handleChangeTheater} name="cars" className="w-[250px] text-[gray] h-10 border rounded text-[16px] bg-slate-200">
                <option value="1">Chọn rạp</option>
                {lodashIsEmpty(theatersData) ? (
                <option value="">Không có rạp...</option>
              ) : (
                theatersData.map((theater) => (
                  <option key={theater.maCumRap} value={theater.maCumRap}>
                    {theater.tenCumRap}
                  </option>
                ))
              )}
            </select>
            <select onChange={handleOnChangeDays} name="cars" className="w-[200px] text-[gray] h-10 border rounded text-[16px] bg-slate-200">
                <option value="Chọn phim">Chọn ngày</option>
                {lodashIsEmpty(theater) ? (
                <option value="">Vui lòng chọn phim, rạp</option>
              ) : (
                Array.from(dateList).map((day, index) => (
                  <option
                    key={index}
                    value={day}
                  >
                    <p style={{ fontSize: "1.2rem", marginBlock: ".2rem" }}>
                    {/* {moment(day.ngayChieuGioChieu).format("DD/MM/YYYY")} */}
                      {dateTime(day).getDateMonth()}
                    </p>
                  </option>
                ))
              )}
            </select>

            <select onChange={handleChangeShowTime} name="cars" className="w-[200px] text-[gray] h-10 border rounded text-[16px] bg-slate-200">
                <option value="Chọn phim">Chọn giờ</option>
                {lodashIsEmpty(showDays) ? (
                <option value="">
                  Vui lòng chọn phim, rạp, ngày chiếu
                </option>
              ) : (
                renderTimes()
              )}
            </select>
            <button onClick={handleBooking} className="text-white bg-gradient-to-r from-[#e0bf93] to-[#d18b44] cursor-pointer py-2 px-4 rounded-full">Đặt vé</button>
        </div>
    </div>
    
  );
};

export default SearchTicket;
