import React from "react";
import { AiOutlineUserAdd } from "react-icons/ai";
import { BiLogOut } from "react-icons/bi";
import { BsEmojiSmile } from "react-icons/bs";
import { FiLogIn } from "react-icons/fi";
import { useDispatch, useSelector } from "react-redux";
import { Link, useNavigate } from "react-router-dom";
import { SET_LOGIN } from "../../../redux copy/constant/userConstant";
import { userInforLocal } from "../../../service/local.sevice";

export default function UserNav() {
  const navigate=useNavigate()
  const dispatch=useDispatch()
  const userInfor=useSelector(state=>state.userReducer.userInfor)
  
  const handleLogout=()=>{
    userInforLocal.remove()
    dispatch({
      type:SET_LOGIN,
      payload:null
    })
    navigate("/")
  }
  return (
    <>
    {userInfor?
      <div className="w-full flex justify-end items-center text-[white] font-bold text-[16px]">
        <span className="">
          <BsEmojiSmile/>
        </span>
        <Link>
          <span className="lg:mx-[18px] md:mx-[14px] mx-[10px] text-[12px] md:text-[14px] lg:text-[16px] cursor-pointer">
            {userInfor.hoTen}<span className="mx-[5px]"></span>|
          </span>
        </Link>
        <span className="">
          <BiLogOut/>
        </span>
        <Link onClick={handleLogout}>
          <span className="lg:mx-[18px] md:mx-[14px] mx-[10px] text-[12px] md:text-[14px] lg:text-[16px] cursor-pointer">
            Đăng xuất <span className="mx-[5px]"></span>|
          </span>
        </Link>
      </div>
      :
      <div className="w-full flex justify-end items-center text-[white] font-bold">
        <span className="">
          <FiLogIn />
        </span>
        <Link to="/dangnhap">
          <span className="lg:mx-[18px] md:mx-[14px] mx-[10px] cursor-pointer text-[12px] md:text-[14px] lg:text-[16px]">
            Đăng nhập <span className="mx-[5px]"></span>|
          </span>
        </Link>
        <span className="">
          <AiOutlineUserAdd />
        </span>
        <Link to="/dangky">
          <span className="lg:mx-[18px] md:mx-[14px] mx-[10px] cursor-pointer text-[12px] md:text-[14px] lg:text-[16px]">
            Đăng ký <span className="mx-[5px]"></span>|
          </span>
        </Link>
      </div>
}
    </>
  );
}
