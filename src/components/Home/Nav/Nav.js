import React, { useState } from "react";

import { FaBars } from "react-icons/fa";
import { Link } from "react-router-dom";
import logo from "../../../images/logo.svg";
import UserNav from "./UserNav";
const Nav = ({setIsShow}) => {
    const [linkid,setLinkid]=useState("")
    const navLinks = ["Trang chủ", "Lịch chiếu", "Cụm rạp", "Comming soon", "Liên hệ"];
    return ( 
        <div className="w-[100%] bg-gradient-to-r from-black to-orange-700" id="Trang chủ">
            <div className="my-0 mx-auto p-2 max-w-[90%] ">
            <UserNav/>
            <div className="flex items-center justify-between">
                <Link to="/"><img src={logo} alt="logo" className="cursor-pointer lg:w-[230px] md:w-[180px] w-[150px]" /></Link>
                <div
                    className="flex md:hidden text-[#fbbd61] text-2xl cursor-pointer z-20"
                    onClick={() => setIsShow((prev) => !prev)}
                >
                    <FaBars />
            </div>
          
            <div className="md:flex hidden">
                <ul className="flex items-center border-b border-white/20 ml-8">
                    {navLinks.map((link) => (
                        <a href={`#${linkid}`}>
                            <li
                            onClick={()=>setLinkid(link)}
                            key={link}
                            className={`link px-5 py-4 uppercase text-[white] font-bold lg:px-6 lg:py-5 lg:text-base cursor-pointer ${
                                link === "Trang chủ" ? "active" : ""
                            }`}
                            >
                            <p>{link}</p>
                            </li>
                        </a>
                    ))}
                    </ul>
                </div>
            </div>
        </div>
        </div>
        
     );
}
 
export default Nav;

