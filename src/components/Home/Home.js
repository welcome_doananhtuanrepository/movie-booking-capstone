import React, { useEffect, useState } from "react";
import ComingSoon from "./ComingSoon";
import Contact from "./Contact";
import Footer from "./Footer";
import Header from "./Header";
import HeaderMobile from "./HeaderMobile/HeaderMobile"
import Nav from "./Nav/Nav";
import ListMovie from "./ListMovie/ListMovie";
import Theater from "./Theater/Theater";
import BacktoTop from "./BacktopTop/BacktoTop";
import SearchTicket from "./SearchTicket/SearchTicket";
function Home() {
  const [isShow, setIsShow] = useState(false);

  
  
  return (
    <div className="flex flex-col items-center relative overflow-hidden">
      <Nav isShow={isShow} setIsShow={setIsShow}/>
      <HeaderMobile isShow={isShow} setIsShow={setIsShow}/>
      <Header setIsShow={setIsShow} />
      <SearchTicket/>
      <ListMovie/>
      <Theater/>
      
      <ComingSoon />
      <Contact />      
      <Footer />
      <BacktoTop/>    
    </div>
  );
}

export default Home;
