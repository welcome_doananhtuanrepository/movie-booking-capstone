import React, { useEffect, useState } from 'react'
import {  message, Radio, Tabs  } from "antd";
import 'antd/dist/antd.css';
import LstCumRap from "./LstCumRap";
import { movieServ } from "../../../service/movie.ser";
import Loading from '../../../pages/loading/Loading';
import "./style.css"
export default function Theater() {
  // const [theaterList,setTheaterList]=useState([])
  const [dataRaw,setDataRaw]=useState([])
  // const [loading,setLoading]=useState(true)
  const fetchMoviesTheaters = () => {
    // setLoading(false)
    movieServ
      .getMoviesbyTheater()
      .then((res) => {
        setDataRaw(res.data.content)
      })
      .catch((err) => {
        // setLoading(false)
        message.error(err.response.data.content);
      });
  };
  useEffect(()=>{
    fetchMoviesTheaters()
  },[])

  return (
    // <>
    // {loading?<Loading/>:
    <>
    <div className='container'>
        <div id="Cụm rạp" className="w-[100%] mx-auto border mt-10">
          <Tabs
            defaultActiveKey="0"
            tabPosition="left"
            style={{
              height: 650,
            }}
            items={dataRaw.map((heThongRap, id) => {
              return {
                label:<img src={heThongRap.logo} alt="" className="w-[30px]"></img>,
                key: `${heThongRap.maHeThongRap}`,
                children: 
                  <LstCumRap
                  lstCumRap={heThongRap.lstCumRap}
                  />,
              };
            })}
          />
        </div>
    </div>
      
    </>
    // }
    
    // </>
    
  )
}
