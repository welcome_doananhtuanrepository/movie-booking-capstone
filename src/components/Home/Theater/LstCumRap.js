import React from 'react'
import {   Tabs  } from "antd";
import 'antd/dist/antd.css';
import LstPhim from './LstPhim';
export default function LstCumRap({lstCumRap}) {
  return (
    <div>
        <Tabs
            defaultActiveKey="0"
            tabPosition="left"
            style={{
                height: 650,
                padding:0,
                // width:300
              }}
            className="p-0"
            items={lstCumRap.map((cumRap, id) => {
        
            return {
                label:  <div className='text-left h-10 p-0 md:w-auto sm:w-[150px] w-[120px]'>
                            <p className='md:text-[16px] sm:text-[14px] text-[10px] mb-0 font-semibold md:w-[250px] sm:w-[180px] w-[130px]'>
                                <span>{cumRap.tenCumRap?.split("-")[0]}</span>
                                <span>-{cumRap.tenCumRap?.split("-")[1]}</span>
                            </p>
                            <p className='md:text-[16px] sm:text-[14px] text-[10px] md:w-[350px] sm:w-[180px] w-[130px]'>{cumRap.diaChi}</p>
                        </div>,
                key: `${cumRap.maCumRap}`,
                children: 
                    <LstPhim
                    lstPhim={cumRap.danhSachPhim} 
                    key={cumRap.maCumRap} 
                    />
            };
            })}
  /></div>
  )
}
