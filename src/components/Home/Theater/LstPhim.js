import React from 'react'
import LstNgayChieu from './LstNgayChieu'


export default function LstPhim({lstPhim}) {
  return (
    <div className='h-[650px] overflow-auto'>
      {lstPhim.map(phim => (
        <div key={phim.maPhim} className="mt-2">
          <div className='sm:flex'>
            <img className='md:w-[100px] md:h-[100px] w-[60px] h-[60px] object-cover' src={phim.hinhAnh} alt={phim.tenPhim} />
            <div className='md:text-[16px] sm:text-[14px] text-[12px] sm:mt-0 mt-2 sm:ml-2 ml-0'>
              <p className='md:text-[20px] sm:text-[14px] text-[10px] mb-[0px] font-semibold'>{phim.tenPhim}</p>
              <p>120 phút- Điểm Tix 10</p> {/* phải tách riêng ra vì thời lượng và đánh giá lấy từ một api khác */}
            </div>
          </div>
          <div>{/* div danh sách ngày giờ chiếu */}
            <LstNgayChieu lstLichChieuTheoPhim={phim.lstLichChieuTheoPhim} />
          </div>
        </div>
      ))}
    </div>
  )
}
