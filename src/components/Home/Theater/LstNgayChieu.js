import React, { Fragment } from 'react'
// import BtnGoToCheckout from './BtnGoToCheckout';
import moment from "moment"
import { useNavigate } from "react-router-dom";
import { useSelector } from 'react-redux';
import { message } from 'antd';
export default function LstNgayChieu({lstLichChieuTheoPhim}) {
  const navigate = useNavigate();
  const userInfo=useSelector(state=>state.userReducer.userInfor)
    const mangChiChuaNgay = lstLichChieuTheoPhim.map(item => {  // tạo mảng mới chỉ chứa ngày

        return item.ngayChieuGioChieu.slice(0, 10);// item là "2020-12-17" cắt ra từ 2020-12-17T10:10:00
    })
    const MangNgayKhongTrungLap = [...new Set(mangChiChuaNgay)] // xóa đi ngày trùng lặp > dùng mảng này để render số phần tử
   
    const filterByDay = (date) => { // lọc ra item từ mảng gốc
        const gioChieuRenDer = lstLichChieuTheoPhim.filter(item => {
          if (item.ngayChieuGioChieu.slice(0, 10) === date) {
            return true
          }
          return false
        })
        return gioChieuRenDer;
      }
    const handleBooking=(maLichChieu)=>{
        if(userInfo){
          navigate(`/datve/${maLichChieu}`)
        }else{
          message.info("Bạn cần đăng nhập để đặt vé")
          navigate("/") 
        }
    }
    return (
        <div>
        {MangNgayKhongTrungLap.map(date =>{
          
          return(
            <Fragment key={date}>
              <p className='mb-0 md:text-[16px] sm:text-[14px] text-[10px] font-semibold text-orange-700'>{moment(date).format("DD/MM/YYYY")}</p> {/*in ra ngày hiện tại*/}
              <div className='flex gap-3 flex-wrap'>
                {filterByDay(date).map(lichChieuTheoPhim => (
                  <Fragment key={lichChieuTheoPhim.maLichChieu}>
                    <button onClick={()=>handleBooking(lichChieuTheoPhim.maLichChieu)} 
                    className='bg-gradient-to-r shadow-md from-[#fbbd61] to-[#ec7532] 
                    md:text-[16px] sm:text-[14px] text-[10px] cursor-pointer md:px-3 sm:px-2 sm:py-1 py-[2px] px-1 rounded-full'>
                      <span className='text-white'>{lichChieuTheoPhim.ngayChieuGioChieu.slice(11, 16)}</span>
                    </button>
                  </Fragment>
                ))}
              </div>
            </Fragment>
          )})
        }
      </div >
  )
}
