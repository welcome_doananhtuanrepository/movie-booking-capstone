import { GrFormNext } from "react-icons/gr";
import { FaUserCircle } from "react-icons/fa";
import logo from "../../../images/logo.svg";
import { Link } from "react-router-dom";
import { useNavigate } from "react-router-dom";
import { useEffect, useState } from "react";
const HeaderMobile = ({ isShow, setIsShow }) => {
  const navigate = useNavigate();
  const [linkid, setLinkid] = useState("");
  const navLinks = ["Lịch chiếu", "Cụm rạp", "Comming soon", "Liên hệ"];

  useEffect(()=>{
    
    const handleClose=()=>{
      setIsShow(false)
    }
    window.addEventListener("scroll", handleClose);
    // return ()=>{
    //   window.removeEventListener("scroll")
    // }
  },[])

  return (
    <>
      <div
        className={`absolute md:hidden top-0 left-0 h-screen bg-slate-700 z-[999999] w-full min-h-full opacity-[0.1] ${
          isShow ? "block" : "hidden"
        } overflow-hidden`}
        onClick={() => setIsShow(false)}
      ></div>
      <div
        className={`w-[320px] translate-x-[100%] min-h-full bg-[#111] md:hidden absolute right-0 z-[999999999] ${
          isShow ? "translate-x-[0%] " : "translate-x-[100%]"
        } `}
      >
        <div className="flex m-[40px] items-center">
          <Link to="/">
            <img
              src={logo}
              alt="logo"
              className="cursor-pointer lg:w-[230px] md:w-[180px] w-[150px]"
            />
          </Link>
          <span
            className="flex ml-[60px] text-[25px] cursor-pointer"
            onClick={() => setIsShow(false)}
          >
            <GrFormNext className="bg-gray-400 rounded-lg" size={"25px"} />
          </span>
        </div>
        <div
          onClick={() => setIsShow(false)}
          className="py-[20px] px-[40px] hover:bg-slate-200 hover:text-[#ec7532] cursor-pointer mb-[10px]"
        >
          <span className="text-[20px] text-[#666]">Trang chủ</span>
        </div>
        {navLinks.map((link) => (
          <a href={`#${linkid}`}>
            <div
              onClick={() => setLinkid(link)}
              key={link}
              className="py-[20px] px-[40px] hover:bg-slate-200 hover:text-[#ec7532] cursor-pointer mb-[10px]">
              <span className="text-[20px] text-[#666]">{link}</span>
            </div>
          </a>
        ))}
      </div>
    </>
  );
};

export default HeaderMobile;
