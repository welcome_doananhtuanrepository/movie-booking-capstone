const currentUser = localStorage.getItem("user")
  ? JSON.parse(localStorage.getItem("user"))
  : null;
const avtIdUser = currentUser ? currentUser?.avtIdUser : "";

export { avtIdUser };
export const BASE_URL = "https://movie0706.cybersoft.edu.vn/api";
export const FAKE_AVATAR = `https://i.pravatar.cc/300?u=${avtIdUser}`;

export const DATE_BEGIN_DANGCHIEU = "2020-01-01"; // format: yyyy-mm-dd
export const DATE_END_DANGCHIEU = "2020-12-01";

export const DATE_BEGIN_SAPCHIEU = "2020-12-02";
export const DATE_END_SAPCHIEU = new Date().toISOString()?.slice(0, 10);

export const arrayGiaVe = [75000, 100000, 120000, 150000];


