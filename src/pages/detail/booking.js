import 'antd/dist/antd.css';
import { Divider, Steps,Input, Radio, Space, message } from 'antd';
import React, { useEffect, useState } from 'react';
import "./booking.css"
import { movieServ } from '../../service/movie.ser';
import SeatDetails from './SeatDetails';
import BookingDetails from './BookingDetails';
import { useParams } from 'react-router-dom';
import Loading from '../loading/Loading';
import { useSelector } from 'react-redux';
const { Step } = Steps;

export default function Booking() {
    const userInfo=useSelector(state=>state.userReducer.userInfor)
    // console.log(userInfo)
    const [current, setCurrent] = useState(0);
    const [checkout,setCheckout]=useState(true)
    const [value, setValue] = useState(0);
    const [ticketRoom, setTicketRoom] = useState([])
    const [current1, setCurrent1] = useState(0)
    const [isloading,setIsLoading]=useState(true)
    const [inforPhim,setInforPhim]=useState({})
    const id=useParams()
 
    const fetchSeatDeatail = () => {
        
        movieServ
          .getSeatDetail(id.maLichChieu)
          .then((res) => {
            setIsLoading(false)
            setTicketRoom(res.data.content.danhSachGhe)
            setInforPhim(res.data.content.thongTinPhim)
          })
          .catch((err) => {
            setIsLoading(false)
            message.error(err.response.data.content);
          });
      };
      useEffect(()=>{
        fetchSeatDeatail()
      },[])
    const onChangeRadio = (e) => {
        setValue(e.target.value);
    };
    const onChange = (value) => {
        setCurrent(value);
      };

    const next = () => {
      console.log(current + 1)
        setCurrent(current + 1);
      }
    const prev = () => {
        setCurrent(current - 1);
    }
  return (
    <div>
      {isloading?<Loading/>:
      <>
       <div className='h-full w-full md:flex'>
        <div className='bg-white md:w-[70%]'>
            <div className='md:h-[70px] h-[150px] p-4 flex justify-between shadow-3xl px-6'>
                <div className="lg:w-[750px] md:w-[680px] sm:w-[520px] w-[450px] flex items-center">
                    <Steps current={current} onChange={onChange}>
                        <Step title="CHỌN GHẾ" />
                        <Step title="THANH TOÁN" />
                        <Step title="KẾT QUẢ ĐẶT VÉ" />
                    </Steps>
                </div>
                <div className="flex items-center justify-center">
                    <div className='flex flex-col items-center space-y-2'>
                        <div><img className='md:w-[25px] md:h-[25px] sm:w-9 sm:h-9 w-11 h-11 rounded-[50%]' src="https://znews-photo.zingcdn.me/w360/Uploaded/qfssu/2022_10_24/312711135_665529898471061_6854198162972919707_n_1.jpg" alt=""/></div>
                        <h1 className='mb-0 text-[12px]'>{userInfo.hoTen}</h1>
                    </div>
                </div>
            </div>
            <div>
                <SeatDetails ticketRoom={ticketRoom} />
            </div>
        </div>
        <div className='md:w-[30%] w-[100%] md:p-0 p-4 md:h-screen'>
            <BookingDetails setCurrent={setCurrent} current={current} inforPhim={inforPhim} next={next}/>
        </div>
      </div>
      </>}
    </div>
    
  )
}
