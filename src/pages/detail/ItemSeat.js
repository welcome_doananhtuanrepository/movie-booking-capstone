import { connect, useDispatch, useSelector } from "react-redux";
import React from "react";
import RegularSeat from "../../components/BookingMovie/RegularSeat";
import VipSeat from "../../components/BookingMovie/VipSeat";
import { SELECT_SEAT } from "../../redux copy/constant/BookingMovie";
import SelectingSeat from "../../components/BookingMovie/SelectingSeat";

function ItemSeat({ seat }) {
   const  dispatch=useDispatch()
  const selectedSeats=useSelector(state=>state.bookingReducer.selectedSeats)
  const renderItemSeat = () => {
    const checkSelectedSeat = (data) => {
      const selectedSeat =  selectedSeats.find((item) => {
        return item.maGhe === data.maGhe;
      });
      return selectedSeat == undefined
    };
    let isSelected = checkSelectedSeat(seat);
    if (seat.loaiGhe === "Thuong") {
      return (
        <span
          onClick={() => {
            dispatch({ type: SELECT_SEAT, payload: seat });
          }}
        >
          {" "}
          {isSelected ? <RegularSeat /> : <SelectingSeat seat={seat} />}
        </span>
      );
    }
    if (seat.loaiGhe === "Vip") {
      return (
        <span
          onClick={() => {
            dispatch({ type: SELECT_SEAT, payload: seat });
          }}
        >
          {isSelected ? <VipSeat /> : <SelectingSeat seat={seat} />}
        </span>
      );
    }
  };
  return <div className="cursor-pointer">{renderItemSeat()}</div>;
}

export default ItemSeat;
