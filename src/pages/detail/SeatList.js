import React from 'react'
import ItemSeat from './ItemSeat'
import SeatLegend from './SeatLegend'

export default function SeatList({ticketRoom}) {
  // console.log(ticketRoom)
  return (
    <div className=''>
      <div className='w-[100%] px-4 mx-auto grid grid-cols-12 md:gap-y-2 gap-y-1 gap-x-2 bg-slate-200 mb-6'>
        {ticketRoom.map((seat)=>{
          return <ItemSeat seat={seat} key={seat.maGhe}/>
        })}
      </div>
    </div>
    
  )
}
