import React from "react";

export default function TheaterInfor() {
  return (
    <div className="flex flex-row justify-between px-32 mt-4">
      <div className="flex flex-row justify-between">
        <img className="w-16 h-16" />
        <div className="ml-4">
        <p>BHD Star Cineplex - 3/2</p>
        <p>Thứ bảy - 11:05 - Rạp 1</p>
        </div>
 
      </div>
      <div>
        <p>Thời gian giữ ghế</p>
        <h1 className="text-lg font-bold">04:36</h1>
      </div>
    </div>
  );
}
