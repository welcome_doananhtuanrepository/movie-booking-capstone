import React from "react";
import { Input, Radio, Space } from "antd";
import { useState } from "react";
import { connect } from "react-redux";
import { SELECT_PAYMENT_METHOD } from "../../redux copy/constant/BookingMovie";

function PaymentMethod({selectPaymentMethod,setByPay}) {
  const [value, setValue] = useState(1);
  const onChange = (e) => {
    setValue(e.target.value);
    selectPaymentMethod(e.target.value);
    setByPay(true)
  };
  return (
    <div className="mb-2">
      <Radio.Group onChange={onChange} value={value}>
        <Space direction="vertical">
          <Radio value="ZaloPay">Thanh toán qua ZaloPay</Radio>
          <Radio value='Visa, Master, JCB'>Visa, Master, JCB</Radio>
          <Radio value='The ATM nội địa'>The ATM nội địa</Radio>
          <Radio value='Thanh toán tại cửa hàng tiện ích'>Thanh toán tại cửa hàng tiện ích</Radio>
        </Space>
      </Radio.Group>
    </div>
  );
}

const mapDispatchToProps = (dispatch)=>{
  return {
    selectPaymentMethod: (value) => dispatch ({
      type: SELECT_PAYMENT_METHOD,
      payload: value,
    })
  }
}

export default connect (null, mapDispatchToProps)(PaymentMethod)