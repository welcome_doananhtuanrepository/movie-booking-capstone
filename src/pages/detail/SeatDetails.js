import React from "react";
import Screen from "./Screen";
import SeatLegend from "./SeatLegend";
import SeatList from "./SeatList";
import TheaterInfor from "./TheaterInfor";

export default function SeatDetails({ticketRoom}) {
 
  return (
    <div className="overflow-auto">
      <SeatLegend />
      <Screen />
      <SeatList ticketRoom={ticketRoom}/>
      {/* <SeatLegend /> */}
    </div>
  );
}