import React, { useEffect, useState } from 'react'
import RegularSeat from '../../components/BookingMovie/RegularSeat'
import SelectingSeat from '../../components/BookingMovie/SelectingSeat'
import UnavaiSeat from '../../components/BookingMovie/UnavaiSeat'
import VipSeat from '../../components/BookingMovie/VipSeat'
import useCountDown from '../../HOC/useCountDownTime'

export default function SeatLegend() {
  const endTime=new Date().getTime()+60000*5;
  const [timeChange,setEndTime]=useCountDown()
  const minutes=Math.floor(timeChange/60000%60);
  const seconds=Math.floor(timeChange/1000%60)

  useEffect(()=>{
    setEndTime(endTime)
  },[])

  
  return (
    <div className='flex justify-between px-12'>
        {/* <div className='absolute top-0 left-0 '></div> */}
        <div className="flex flex-row justify-evenly w-[50%] mt-4 mx-auto">
          {/* unAvaiSeat */}
          <div>
            <UnavaiSeat />
            <p className='text-[12px] font-semibold text-center'>Đã mua</p>
          </div>

          {/* selectingSeat */}
          <div>
            <SelectingSeat />
            <p className='text-[12px] font-semibold text-center'>Đang mua</p>
          </div>

          {/* vipSeat */}
          <div>
            <VipSeat />
            <p className='text-[12px] font-semibold text-center'>VIP</p>
          </div>

          {/* regularSeat */}
          <div>   
            <RegularSeat />
            <p className='text-[12px] font-semibold text-center'>Thường</p>
          </div>
        </div>
        <div className='w-[50%] text-center mt-4'>
          <h4>Thời gian giữ ghế</h4>
          <h1 className='text-lg font-semibold'>{minutes}:{seconds}</h1>
        </div>
    </div>
    
  )
}
