import React, { useState } from "react";
import { Divider } from "antd";
import { useNavigate } from "react-router-dom";
import { connect, useDispatch, useSelector } from "react-redux";
import PrimaryButton from "../../components/BookingMovie/PrimaryButton";
// import DisabledButton from "../../components/BookingMovie/DisabledButton";
import PaymentMethod from "./PaymentMethod";
import DisabledButton from "../../components/BookingMovie/DisabledButton";
import CheckoutButton from "../../components/BookingMovie/CheckoutButton";
import { SELECT_SEAT } from "../../redux copy/constant/BookingMovie";
import Checkout from "../../components/Checkout/Checkout";

function BookingDetails({current,next,inforPhim,setCurrent}) {
  const dispatch=useDispatch()
  const navigate=useNavigate();
  const totalAmount=useSelector(state=>state.bookingReducer.totalAmount)
  const selectedSeats=useSelector(state=>state.bookingReducer.selectedSeats)
  const userInfo=useSelector(state=>state.userReducer.userInfor)
  const [byPay,setByPay]=useState(false)
  // const handleConfirm=()=>{
  //   console.log("kadl.ádm")
    
  //   setCurrent(0)
  //   navigate("/")
  //   dispatch({ type: SELECT_SEAT, payload: null });
    
  // }
  // console.log(current)
  return (
    <>
      {current===2&&<Checkout setCurrent={setCurrent} inforPhim={inforPhim}/>}
      <div className="md:w-[100%] sm:w-[80%] w-[70%] mx-auto h-full bg-orange-50 shadow-3xl px-6 md:py-0 py-2">
        <h1 className="text-center text-[#f5b640] md:text-[40px] sm:text-[35px] text-[30px] md:pt-[24px] sm:pt-[20px] pt-[18px] mb-0">
          {totalAmount}đ
        </h1>
        <Divider className="md:my-[12px] my-[8px]" />
        <div className="flex flex-col gap-2">
          <div>
            <span className="text-[15px] font-semibold">Tên phim: </span><span>{inforPhim.tenPhim}</span>
          </div>
          <div>
            <span className="text-[15px] font-semibold">Địa chỉ: </span><span>{inforPhim.diaChi}</span>
          </div>
          <div>
            <span className="text-[15px] font-semibold">Thời gian: </span><span>{inforPhim.gioChieu} - {inforPhim.ngayChieu}</span>
          </div>
          
        </div>
        <Divider />
        <h1 className="text-[15px] font-semibold">Ghế: </h1>
        <div className="flex flex-row justify-between">
          <p className="font-medium text-lg">
            {selectedSeats.map((item) => {
              let content = "";
              let contentHTML = "";
              content = `Ghế ${item.tenGhe}, `;
              return (contentHTML += content);
            })}
          </p>
          <p className="font-bold text-lg text-orange-300">{totalAmount}đ</p>
        </div>
        <Divider />
        <span className="text-[15px] font-semibold">Email: </span><span>{userInfo.email}</span>
        
        <Divider />
        <span className="text-[15px] font-semibold">Phone: </span><span>{userInfo.soDT}</span>
        <Divider />
        <p className="text-[15px] font-semibold mb-2">Hình thức thanh toán</p>
        {selectedSeats.length === 0 ? (
          <span className="text-red-700 text-[14px]">
            Vui lòng chọn ghế để hiển thị hình thức thanh toán phù hợp
          </span>
        ) : (
          <PaymentMethod setByPay={setByPay}/>
        )}
        {current===0&&byPay===false&&<DisabledButton />}
        {current===0&&byPay===true&&<PrimaryButton next={next}/>}
        {current===1&&byPay===true&&<CheckoutButton next={next}/>}
      </div>
    </>
    
  );
}

export default BookingDetails;
