import React from "react"; 
import logo from "../../../src/images/logo.svg";
import { AiOutlineCloseCircle} from "react-icons/ai"
import {useNavigate} from "react-router-dom"
import { SET_LOGIN } from '../../redux copy/constant/userConstant';
import Loading from "../loading/Loading";
import { userServ } from '../../service/userServ';
import { Button, Form, Input, message } from "antd";
import { userInforLocal } from '../../service/local.sevice';
import "./Login.css"
import { useDispatch } from "react-redux";

const Login = () => {
    const dispatch=useDispatch()
 
    const navigate=useNavigate();
    const handleNavigate=()=>{
        navigate("/")
    }
    const onFinish = (values) => {
      userServ
        .postLogin(values)
        .then((res) => {
          console.log(res);
          // lưu thông tin đăng nhập xuống localStorage
          userInforLocal.set(res.data.content);
          dispatch({
            type: SET_LOGIN,
            payload: res.data.content,
          });
          message.success("Đăng nhập thành công");
          setTimeout(() => {
            navigate("/");
          }, 1000);
        })
        .catch((err) => {
          message.error(err.response.data.content);
          console.log(err);
        });
    };
    const onFinishFailed = (errorInfo) => {
      console.log('Failed:', errorInfo);
    };
    return ( 
        <div className="w-full h-screen">
            <div className="w-full bg-cover bg-hero4 bg-center flex justify-center">
            <div className="w-[full] h-screen"></div>
            <div className="w-[600px] h-[480px] rounded-md login relative my-auto">
                <div className="mt-6">
                    <img src={logo} alt="logo" className="cursor-pointer mx-auto" />
                </div>
                <p className="text-white text-center mt-4">Thế giới phim trên đầu ngón tay</p>
                <p className="text-white text-center mt-5 text-[18px]">Đăng Ký để được nhiều ưu đãi, mua vé và bảo mật thông tin!</p>
                <Form
                    layout="vertical"
                    name="basic"
                    labelCol={{ span: 4 }}
                    wrapperCol={{ span: 24 }}
                    initialValues={{ remember: true }}
                    onFinish={onFinish}
                    onFinishFailed={onFinishFailed}
                    autoComplete="off"
                >
                <Form.Item
                    label="Tài khoản"
                    name="taiKhoan"
                    rules={[{ required: true, message: 'Trường này không được để rỗng!' }]}
                >
                <Input />
                </Form.Item>

                <Form.Item
                    label="Mật khẩu"
                    name="matKhau"
                    rules={[{ required: true, message: 'Trường này không được để rỗng!' }]}
                >
                    <Input.Password />
                </Form.Item>

                <Form.Item wrapperCol={{ offset: 10, span: 24 }}>
                    <Button htmlType="submit">
                        Submit
                    </Button>
                </Form.Item>
                </Form>
                    <div className="text-white absolute top-0 right-0 cursor-pointer" onClick={handleNavigate}>
                            <AiOutlineCloseCircle size={"30px"}/>
                    </div>
                </div>
                
                </div>
            </div>
        
     );
}
 
export default Login;