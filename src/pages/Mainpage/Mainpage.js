
import Layout, { Content, Header } from "antd/lib/layout/layout";
import { useState } from "react";
import HeaderApp from "../../components/Admin/HeaderApp";
import SideBar from "../../components/Admin/SideBar";
import MovieManagment from "../MovieManagement/MovieManagment";
import "../UserManagement/UserManagement.css";
import UserManagementPage from "../UserManagement/UserManagementPage";


export default function MainPage() {
  const [currentKey, setCurrentKey] = useState()
    const getCurrentKey = (data) => {
        setCurrentKey(data)
    }
   
  return (
    <div className="w-screen">
      <Layout>
        <SideBar getCurrentKey={getCurrentKey} />
        <Layout>
          <HeaderApp />
          <Content
            style={{
              margin: "24px 16px 0",
            }}
          >
            <div
              className="site-layout-background"
              style={{
                padding: 24,
                minHeight: 360,
              }}
            >
              {currentKey === "2" ? <MovieManagment /> : <UserManagementPage />}
            </div>
          </Content>
        </Layout>
      </Layout>
    </div>
  );
}