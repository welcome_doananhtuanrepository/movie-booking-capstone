import React, { useEffect, useState } from 'react'  
import {  Tabs  } from "antd";
import 'antd/dist/antd.css';
import TimeStart from './TimeStart';
export default function LichChieu({heThongRapChieu}) {
  return (
    <div className='border my-6 text-[white] bg-white rounded-lg'>
        <Tabs
        defaultActiveKey="0"
        tabPosition="left"
        className='w-full h-[550px]'
        items={heThongRapChieu.map((cumRap, id) => {
          return {
            label:
            <div className='flex items-center space-x-2'>
              <img src={cumRap.logo} alt="" className="lg:w-[50px] md:w-[38px] sm:w-[28px] w-[20px]"/>
              <span className='md:text-[16px] text-[12px] '>{cumRap.maHeThongRap}</span>
            </div>,
            key: `${cumRap.maHeThongRap}`,
            children: 
              <div>
                  <div key={cumRap.maHeThongRap}>
                    {cumRap.cumRapChieu.map((thongtincumrap,index)=>(
                      <TimeStart key={index} cumRapChieu={thongtincumrap.lichChieuPhim} thongtincumrap={thongtincumrap}/>
                    ))}
                  </div>
              </div>
          };
        })}
      />
    </div>
  )
}

