import React from 'react'
import moment from 'moment';
export default function ThongTin({dataMoives}) {
  console.log(dataMoives)
  return (
    <div className='sm:flex justify-between my-6 px-6'>
        <div className='sm:w-[50%] w-full text-white text-[16px]'>
            <p><span>Ngày công chiếu:</span><span className='ml-1'>{moment(dataMoives.ngayKhoiChieu).format("DD/MM/YYYY")}</span></p>
            <p><span>Đạo diễn:</span><span className='ml-1'>Adam Wingard</span></p>
            <p><span>Diễn viên:</span><span className='ml-1'>Kyle Chandler, Rebecca Hall, Eiza González, Millie Bobby Brown</span></p>
            <p><span>Thể Loại:</span><span className='ml-1'>Hành động, giả tưởng, ly kỳ, thần thoại</span></p>
            <p><span>Định dạng:</span><span className='ml-1'>2D/Digital</span></p>
            <p><span>Quốc Gia SX:</span><span className='ml-1'>Mỹ</span></p>
        </div>
        <div className='sm:w-[50%] w-full text-[white]'>
            <h3 className='text-[22px] text-white'>Nội dung</h3>
            <p className='text-[18px]'>{dataMoives.moTa}
            </p>
        </div>
    </div>
  )
}
