import React, { useEffect, useState } from 'react'
import discussData from "../../constant/discussionData"
import {BiLike} from "react-icons/bi"
import moment from "moment";
import "moment/locale/vi";
moment.locale("vi");
export default function Danhgia() {
  const [discuss,setDiscuss]=useState([])
  const [number,setNumber]=useState(1)
  const [showBtnMore,setShowBtnMore]=useState(true)
  const lengthData=discussData.length
  
  const handleShowMore=()=>{
    if(number>Math.round(lengthData/4)){
      setShowBtnMore(false)
    }else{
      setNumber(number+1)
    }
  }
  useEffect(()=>{
    const arr=discussData.slice(0,number*4)
    setDiscuss(arr)
  },[number])
  return (
    <div className='w-full  my-6 flex flex-col items-center'>
        <div className='w-[80%] px-10 flex bg-white mx-auto py-2 rounded-lg'>
          <img className='w-[40px] rounded-full mr-2' src="https://i.pravatar.cc/300?u=V1204254" alt=''/>
          <input className='w-[600px] outline-none' type="text" placeholder="Bạn nghĩ gì về phim này?"></input>
        </div>
        {discuss.map((item,index)=>(
          <div className='w-[80%] bg-white mx-auto rounded-lg px-10 my-6 py-4' key={index}>
            <div className='flex items-center'>
              <img className='w-[30px] h-[30px] rounded-full mr-2' src={`https://i.pravatar.cc/300?u=${item.avtId}`} alt=''/>
              <div>
                <p className='mb-0 font-semibold text-[14px]'>{item.username}</p>
                <p className='text-[12px] text-gray-400'>{moment(`${String(item.createdAt)}`,"YYYYMMDD").fromNow()}</p>
              </div>
            </div>
            <p className='md:text-[17px] sm:text-[15px] text-[13px]'>{item.post}</p>
            <hr/>
            <div className='flex items-center text-[16px] space-x-3'>
              <span className='cursor-pointer'><BiLike/></span><span>{item.likes} thích</span>
            </div>
        </div>
        ))}
        
        {showBtnMore&&<button onClick={handleShowMore} className='px-3 py-2 rounded-lg bg-slate-400 text-[18px] text-[white]'>Load more</button>}
    </div>
  )
}
