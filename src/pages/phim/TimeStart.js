import React, { Fragment, useState} from 'react'
import {   message, Tabs  } from "antd";
import 'antd/dist/antd.css';
import "./TimeStart.css"
import moment from 'moment';
import { useNavigate } from 'react-router-dom';
import { useSelector } from 'react-redux';
export default function TimeStart({cumRapChieu,thongtincumrap}) {

  const navigate = useNavigate();
  const userInfo=useSelector(state=>state.userReducer.userInfor)

  const handleBooking=(maLichChieu)=>{
    if(userInfo){
      navigate(`/datve/${maLichChieu}`)
    }else{
      message.info("Bạn cần đăng nhập để đặt vé")
      return
    }
  }
  const mangChiChuaNgay = cumRapChieu.map(item => {  // tạo mảng mới chỉ chứa ngày
    return item.ngayChieuGioChieu.slice(0, 10);// item là "2020-12-17" cắt ra từ 2020-12-17T10:10:00
  })
  const MangNgayKhongTrungLap = [...new Set(mangChiChuaNgay)] // xóa đi ngày trùng lặp > dùng mảng này để render số phần tử
  
  const filterByDay = (date) => { // lọc ra item từ mảng gốc
    const gioChieuRenDer = cumRapChieu.filter(item => {
      if (item.ngayChieuGioChieu.slice(0, 10) === date) {
        return true
      }
      return false
    })
    return gioChieuRenDer;
  }

  return (
    
    <div>
      <Tabs
        defaultActiveKey="0"
        tabPosition="top"
        style={{
          marginBottom: 8,
          textSize:22,
          height:550
        }}
        items={MangNgayKhongTrungLap.map((date, i) => {
          const id = String(i);
          return {
            label: 
            <div>
              <p className='mb-0 lg:text-[18px] md:text-[16px] sm:text-[14px] text-[12px]'>{moment(date).format("DD/MM/YYYY")}</p>
            </div>,
            key: id,
            children: 
              <div>
                  <div>
                    <div className='md:flex  md:space-x-3 mt-3'>
                      <img className='md:w-[60px] md:h-16 w-[45px] h-12' src={thongtincumrap.hinhAnh} alt=""/>
                      <div className='md:mt-0 mt-2'>
                        <p className='md:text-[16px] text-[14px] font-semibold mb-1'>{thongtincumrap.tenCumRap}</p>
                        <p>{thongtincumrap.diaChi}</p>
                      </div>
                    </div>
                    <div className='flex mt-4 flex-wrap gap-3 items-center'>
                      <h6 className='md:text-[16px] text-[12px] font-bold'>Suất chiếu :</h6>
                    {filterByDay(date).map(lichChieuTheoPhim => (
                      <Fragment key={lichChieuTheoPhim.maLichChieu}>
                        <button onClick={()=>handleBooking(lichChieuTheoPhim.maLichChieu)} className='bg-gradient-to-r shadow-md from-[#fbbd61] to-[#ec7532] cursor-pointer py-1 px-3 rounded-full'>
                          <span className="">{lichChieuTheoPhim.ngayChieuGioChieu.slice(11, 16)}</span>
                        </button>
                      </Fragment>
                    ))}
                    </div>
                  </div>
                
              </div>
            
          };
        })}
      />
    </div>
  )
}