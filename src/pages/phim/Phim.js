import React, { useState,useEffect } from "react";
import { AiFillStar, AiOutlinePlayCircle } from "react-icons/ai";
import Footer from "../../components/Home/Footer";
import Nav from "../../components/Home/Nav/Nav";
import Danhgia from "./Danhgia";
import LichChieu from "./Lichchieu";
import ThongTin from "./ThongTin";
import { useParams } from "react-router-dom";
import moment from "moment"
import Loading from "../loading/Loading";
import { movieServ } from "../../service/movie.ser";
import { message } from "antd";
export default function Phim() {
  const info=useParams()
  const [isShowVideo, setIsShowVideo] = useState(false);
  const [dataMoives,setDataMovies]=useState({})
  const [isloading,setIsLoading]=useState(true)
  
  useEffect(() => {
    setIsLoading(true)
    movieServ.getMoviesDetail(info.maPhim)
    .then((res) => {
            setIsLoading(false)
            setDataMovies(res.data.content)
          })
          .catch((err) => {
            setIsLoading(false)
            message.error(err.response.data.content);
          });
  }, []);
  const [activeTabBtn,setActiveTabBtn]=useState("Lịch chiếu")
  const Tabs=["Lịch chiếu","Thông tin","Đánh giá"]
  
  
  return (
    
    <div>
      {isloading?<Loading/>:
      <>
        {isShowVideo && (
          <div
            onClick={() => setIsShowVideo(false)}
            className="fixed inset-0 bg-black/80 z-30 flex justify-center items-center"
          >
            <div className="relative lg:w-[960px] lg:h-[540px] md:w-[640px] md:h-[360px] h-[260px] w-full p-8">
              <iframe
                className="absolute inset-0 w-[calc(100%-2rem)] h-full left-1/2 transform -translate-x-1/2"
                src={`${dataMoives.trailer}`}
                title="YouTube video player"
                frameBorder="0"
                allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                allowFullScreen
              ></iframe>
            </div>
          </div>
        )}
        <Nav />
        <div className="bg-[#0a2029]">
          <div className="w-[100%] h-[41vw] relative mx-auto">
            <div className={`absolute top-0 bottom-0 left-0 right-0 bg-no-repeat blur-sm bg-cover bg-center bg-[url('https://img6.thuthuatphanmem.vn/uploads/2022/03/16/background-phim-doat-giai_110939053.jpg')]`}></div>
            <div className="text-[#e9e9e9] h-[41vw] flex container mx-auto items-center">
              <div className="group relative h-[70%] w-[40%] flex items-center justify-center mr-6 ml-6 sm:ml-0">
                <img className="object-cover h-[100%] w-[70%] sm:w-[80%] " src={`${dataMoives.hinhAnh}`} alt=""/>
                <img onClick={()=>setIsShowVideo(true)} src="/img/carousel/play-video.png" alt="" className="sm:w-[20%] w-[40px] group-hover:block hidden absolute top-[50%] left-[50%] translate-x-[-50%] translate-y-[-50%] cursor-pointer" size={"60px"}/>
              </div>
              <div className="relative flex flex-col md:space-y-4 sm:w-[50%] sm:space-y-2">
                  <p className="md:text-[18px] mb-0 sm:text-[16px] ">{moment(dataMoives.ngayKhoiChieu).format("DD/MM/YYYY")}</p>
                  <h3 className="md:text-[28px] sm:text-[22px] text-[18px] text-white">{dataMoives.tenPhim}</h3>
                  <p className="md:text-[18px] sm:text-[16px] text-[14px]">120 phút - 10 Tix - 2D/Digital</p>
                  <a href="#content"><button className="md:text-[24px] sm:text-[20px] text-[18px]  md:px-2 sm:px-1 md:py-2 px-0 py-0 sm:py-1 w-[80%] sm:w-[30%] bg-blue-500 rounded-md cursor-pointer">Mua vé</button></a>
              </div>
              <div className="relative w-[20%] md:flex flex-col items-center hidden">
                  <div className="w-[150px] h-[150px] border-[14px] border-green-600 rounded-[50%] flex justify-center items-center">
                    <h1 className="text-[50px] mb-0 text-[white]">{dataMoives.danhGia}/10</h1>
                  </div>
                  <div className="flex">
                      <AiFillStar className="text-[yellow]" size={"26px"}/>
                      <AiFillStar className="text-[yellow]" size={"26px"}/>
                      <AiFillStar className="text-[yellow]" size={"26px"}/>
                      <AiFillStar className="text-[yellow]" size={"26px"}/>
                      <AiFillStar className="text-[yellow]" size={"26px"}/>
                  </div>
                  <p>15 người đánh giá</p>
              </div>
            </div>
          </div>
          <div className="container mx-auto">
            <div className='flex items-center justify-center flex-col'>
                <div className='flex text-center mt-6 space-x-8 sm:text-[24px] text-[18px]'>
                  {Tabs.map((tab,index)=>(
                    <h2 key={index} onClick={()=>setActiveTabBtn(tab)} 
                        className={`tabButton sm:hover:text-[26px] hover:text-[20px] ${activeTabBtn===tab?"text-orange-600":"text-[white]"}`}>
                        {tab} 
                    </h2>
                  ))}
                </div>
                <div id="content" className="container px-3">
                  {activeTabBtn==="Lịch chiếu"&&<LichChieu heThongRapChieu={dataMoives.heThongRapChieu}/>}
                  {activeTabBtn==="Thông tin"&&<ThongTin dataMoives={dataMoives}/>}
                  {activeTabBtn==="Đánh giá"&&<Danhgia/>}
                </div>
            </div>
          </div>
        </div>
        <Footer />
      </>
       }
    </div>
  );
}
