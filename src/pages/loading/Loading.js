import React from "react";
import { SpinnerDotted } from 'spinners-react';
const Loading = () => {
    return ( 
        <div className="absolute top-0 left-0 w-full h-screen flex items-center justify-center">
            <SpinnerDotted size={150} thickness={150} speed={100} color="black" />
        </div>
       
     );
}
 
export default Loading;