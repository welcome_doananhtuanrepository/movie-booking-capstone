import React from "react"; 
import logo from "../../../src/images/logo.svg";

import {AiOutlineCloseCircle} from "react-icons/ai"
import {useNavigate} from "react-router-dom"
import { useDispatch } from "react-redux";

import { Button, Form, Input, message } from "antd";
import { userServ } from "../../service/userServ";
import { SET_SIGNIN } from "../../redux copy/constant/userConstant";
const SignIn = () => {
    const dispatch=useDispatch()
    const navigate=useNavigate();
    const handleNavigate=()=>{
        navigate("/")
    }
    const onFinish = (values) => {
      userServ
        .postSign(values)
        .then((res) => {
          console.log(res);
          
          dispatch({
            type: SET_SIGNIN,
            payload: res.data.content,
          });
          message.success("Đăng ký thành công");
          setTimeout(() => {
            navigate("/dangnhap");
          }, 1000);
        })
        .catch((err) => {
          message.error(err.response.data.content);
          console.log(err);
        });
    };
     
      const onFinishFailed = (errorInfo) => {
        console.log('Failed:', errorInfo);
      };
    return ( 
        <div className="w-full h-full bg-cover bg-hero4 bg-center flex pt-10 justify-center">
            <div className="signin relative">
                <div className="mt-6">
                    <img src={logo} alt="logo" className="cursor-pointer mx-auto" />
                </div>
                <p className="text-white text-center mt-4">Thế giới phim trên đầu ngón tay</p>
                <p className="text-white text-center mt-5 text-[18px]">Đăng Ký để được nhiều ưu đãi, mua vé và bảo mật thông tin!</p>
                <Form
                    layout="vertical"
                    name="basic"
                    labelCol={{ span: 4 }}
                    wrapperCol={{ span: 24 }}
                    initialValues={{ remember: true }}
                    onFinish={onFinish}
                    onFinishFailed={onFinishFailed}
                    autoComplete="off"
                >
                <Form.Item
                    label="Tài khoản"
                    name="taiKhoan"
                    rules={[{ required: true, message: 'Trường này không được để rỗng!' }]}
                >
                <Input />
                </Form.Item>

                <Form.Item
                    label="Mật khẩu"
                    name="matKhau"
                    rules={[{ required: true, message: 'Trường này không được để rỗng!' }]}
                >
                    <Input.Password />
                </Form.Item>

                <Form.Item
                    label="Họ và tên"
                    name="hoTen"
                    rules={[{ required: true, message: 'Trường này không được để rỗng!' }]}
                >
                    <Input />
                </Form.Item>

                <Form.Item
                    label="Email"
                    name="email"
                    rules={[{ required: true, message: 'Trường này không được để rỗng!' }]}
                >
                    <Input />
                </Form.Item>

                <Form.Item
                    label="Phone"
                    name="soDt"
                    rules={[{ required: true, message: 'Trường này không được để rỗng!' }]}
                >
                    <Input />
                </Form.Item>

                <Form.Item wrapperCol={{ offset: 10, span: 24 }}>
                    <Button htmlType="submit">
                        Gửi
                    </Button>
                </Form.Item>
                </Form>
                <div className="text-white absolute top-0 right-0 cursor-pointer" onClick={handleNavigate}>
                        <AiOutlineCloseCircle size={"30px"}/>
                </div>
            </div>
            
        </div>
     );
}
 
export default SignIn;