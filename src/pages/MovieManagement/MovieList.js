import { Button, Image, Space, Table } from 'antd'
import React, { useEffect, useState } from 'react'
import { movieServ } from '../../service/movie.ser'

export default function MovieList() {
    const[movieList,setMovieList] = useState([])
 const fetchMovieList = () => {
  movieServ
    .getListMovies()
    .then((res)=>{
        console.log(res)
        setMovieList(res.data.content)
    })
    .catch((err)=>{
        console.log(err)
    })
 }
 useEffect(()=>{
    fetchMovieList()
 },[])
 const columns = [
    {
      title: 'Tên phim',
      dataIndex: 'tenPhim',
      key: 'tenPhim',
    },
    {
        title: 'Hình ảnh',
        // key: 'hinhAnh',
        dataIndex: 'hinhAnh',
        render: (_,img) => <Image preview={false} width={100} height={120} src={img.hinhAnh}/>
      },
    {
      title: 'Mã phim',
      dataIndex: 'maPhim',
      key: 'maPhim',
    },
    {
      title: 'Mô tả',
      dataIndex: 'moTa',
      key: 'moTa',
    },
    {
      title: 'Thao tác',
      dataIndex: '',
    //   key: '',
      render: ()=>(<Space size="middle">
        
      <Button>Sửa</Button>
      <Button danger>Xóa</Button>
  
  </Space>)
    },
  ];
  return (
    <div><Table columns={columns} dataSource={movieList} /></div>
  )
}
