import React, { useState } from "react";
import { Button, DatePicker, Form, Input, Modal, Switch, Upload } from "antd";
import { UploadOutlined } from "@ant-design/icons";
import { movieServ } from '../../service/movie.ser'

export default function NewMovie({isOpen,setModalClose}) {

  // const [isModalOpen, setIsModalOpen] = useState(true);
  const [fileList, setFileList] = useState([]);


 

  // const showModal = () => {
  //   setIsModalOpen(isOpen);
  // };
  // const handleOk = () => {
  //   setIsModalOpen(false);
  // };
  // const handleCancel = () => {
  //   setIsModalOpen(false);
  // };
  const onFinish = (values) => {
    console.log("Received values of form: ", values);
    console.log("FileList", fileList)
    const imgFile = fileList[0]
    const dataForm = {...values, hinhAnh: imgFile, maNhom: "GP01" }
    console.log("dataForm", dataForm);
    movieServ.postMovie(dataForm)
    
  };
  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };
  const props = {
    onRemove: (file) => {
      const index = fileList.indexOf(file);
      const newFileList = fileList.slice();
      newFileList.splice(index, 1);
      setFileList(newFileList);
    },
    beforeUpload: (file) => {
      setFileList([...fileList, file]);
      return false;
    },
    fileList,
  };
  return (
    <>
      <Modal
        title="Thêm mới phim"
        open={isOpen}
        // onOk={}
        onCancel={()=>{setModalClose(false)}}
      >
        <h1>Thêm mới phim</h1>
        <Form
          name="basic"
          labelCol={{
            span: 8,
          }}
          wrapperCol={{
            span: 16,
          }}
          initialValues={{
            remember: true,
          }}
          onFinish={onFinish}
          onFinishFailed={onFinishFailed}
          autoComplete="off"
        >


          <Form.Item
            label="Tên phim"
            name="tenPhim"
            rules={[
              {
                required: true,
                message: "Nhập tên phim",
              },
            ]}
          >
            <Input />
          </Form.Item>

          <Form.Item
            label="Trailer"
            name="trailer"
            rules={[
              {
                required: true,
              },
            ]}
          >
            <Input />
          </Form.Item>

          <Form.Item
            label="Mô tả"
            name="moTa"
            rules={[
              {
                required: true,
              },
            ]}
          >
            <Input />
          </Form.Item>

          <Form.Item label="Ngày khởi chiếu" name="ngayKhoiChieu" rules={[
              {
                required: true,
              },
            ]}>
            <DatePicker />
          </Form.Item>

          <Form.Item
            label="Đang chiếu"
            valuePropName="checked"
            name="DangChieu"
          >
            <Switch />
          </Form.Item>

          <Form.Item label="Sắp chiếu" valuePropName="checked" name="SapChieu">
            <Switch />
          </Form.Item>

          <Form.Item label="Hot" valuePropName="checked" name="Hot">
            <Switch />
          </Form.Item>

          <Form.Item
            label="Số sao"
            name="danhGia"
            rules={[
              {
                required: false,
              },
            ]}
          >
            <Input />
          </Form.Item>

          <Upload {...props}>
          <Button icon={<UploadOutlined />}>Select File</Button>
          </Upload>

          <Form.Item
            wrapperCol={{
              offset: 8,
              span: 16,
            }}
          >
            <Button type="primary" htmlType="submit" >
              Thêm phim
            </Button>
          </Form.Item>

        </Form>
      </Modal>
    </>
  );
}

