import { Button } from 'antd'
import React, { useState } from 'react'
import MovieList from './MovieList'
import NewMovie from './NewMovie'

export default function MovieManagment() {
  const [isOpen, setIsOpen] = useState(false)
  const setModalOpen = () => {
    setIsOpen(true)
  }
  const setModalClose = (data) => {
    setIsOpen(data)
  }
  return (
    <div>
              <p className="text-xl font-medium">Quản Lý Phim</p>
              <Button type="primary" onClick={()=>{setModalOpen()}} >Thêm phim</Button>
              {isOpen===true && <NewMovie isOpen={isOpen} setModalClose={setModalClose}/>}
              <MovieList />
    </div>
  )
}
