import { Button } from "antd";
import "../UserManagement/UserManagement.css";
import UserList from "./UserList";

export default function UserManagementPage() {
  return (
    <div>
      <p className="text-xl font-medium">Quản Lý Người Dùng</p>
      <Button type="primary">Thêm người dùng</Button>
      <UserList />
    </div>
  );
}
