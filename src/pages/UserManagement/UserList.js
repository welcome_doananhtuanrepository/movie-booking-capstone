import userEvent from '@testing-library/user-event'
import React, { useEffect, useState } from 'react'
import { userServ } from '../../service/userServ'
import { Button, Space, Table, Tag } from 'antd';

export default function UserList() {
    const [userList,setUserList] = useState([])
    console.log("userList", userList)
    let fetchUserList = () => {
        console.log("Yes")
        userServ
        .getUserList()
        .then((res)=>{
            console.log(res)
            setUserList(res.data.content)
        })
        .catch((err)=>{
            console.log(err)
        })
    }
    useEffect(()=>{
        fetchUserList()
    },[])
    const columns = [
        {
          title: 'Email',
          dataIndex: 'email',
          key: 'email',
        },
        {
          title: 'Họ và tên',
          dataIndex: 'hoTen',
          key: 'hoTen',
        },
        {
          title: 'Mật khẩu',
          dataIndex: 'matKhau',
          key: 'matKhau',
        },
        {
          title: 'Số điện thoại',
          key: 'soDT',
          dataIndex: 'soDT',
        },
        {
            title: 'Mã loại người dùng',
            key: 'maLoaiNguoiDung',
            dataIndex: 'maLoaiNguoiDung',
          },
        {
          title: 'Thao tác',
          key: 'action',
          render: ()=>(
            <Space size="middle">
                <Button>Sửa</Button>
                <Button danger>Xóa</Button>
              </Space>
          )
        },
      ];
  return (
    <div><Table columns={columns} dataSource={userList} /></div>
  )
}
