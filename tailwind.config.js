module.exports = {
  content: ["./src/**/*.{html,js}"],
  theme: {
    screens: {
      'sm': '576px',
      'md': '960px',
      'lg': '1140px',
    },
    extend: {
      fontFamily: {
        main: ["Roboto Condensed", "sans-serif"],
      },
      boxShadow: {
        '3xl': '0 0 15px rgb(0 0 0 / 30%)',
      },
      backgroundImage: {
        // hero1: "url('/src/assets/img/carousel/gai-gia-lam-chieu.jpg')",
        // hero2: "url('/src/assets/img/carousel/lua-deu-gap-lua-dao.jpg')",
        // hero3: "url('/src/assets/img/carousel/than-bip-jack-chot.jpg')",
        hero4: "url('/src/assets/img/signin/bgAuth.jpg')",
      },
      keyframes: {
        "fade-in-down": {
          "0%": {
            opacity: "0",
            transform: "translateY(-50px)",
          },
          "100%": {
            opacity: "1",
            transform: "translateY(0)",
          },
        },
        "fade-in-up": {
          "0%": {
            opacity: "0",
            transform: "translateY(50px)",
          },
          "100%": {
            opacity: "1",
            transform: "translateY(0)",
          },
        },
      },
      animation: {
        "fade-in-down": "fade-in-down 0.5s ease-out forwards",
        "fade-in-down-delay": "fade-in-down 0.8s ease-out forwards",
        "fade-in-up": "fade-in-up 0.5s ease-out forwards",
        "fade-in-up-delay": "fade-in-up 0.8s ease-out forwards",
      },
    },
  },
  plugins: [],
};
